%include "lib.inc" 

%define OFFSET 8

global find_word

section .text
find_word:
    push rdi
    push rsi
    add rsi, OFFSET
    call string_equals
    pop rsi
    pop rdi

    test rax, rax
    jne .found

    mov rsi, [rsi]
    test rsi, rsi
    jne find_word ; loop if haven't matched

    xor rax, rax
    ret

.found:
    mov rax, rsi
    ret
