%include "colon.inc"
%include "words.inc"
%include "lib.inc"
extern find_word

%define BUFFER_CAPACITY 255
%ifndef OFFSET
	%define OFFSET 8
%endif

section .rodata
greeting: db "Insert string key for searching in linked list", 0
found: db "Here is value found by inserted key:", 0
notfound: db "There is no value for inserted key.", 0
overflow: db "Warning! Key length should be less than 255 chars.", 0

section .bss
input: times BUFFER_CAPACITY db 0

section .text
global _start

_start:
	xor rax, rax
	xor rdi, rdi 

	mov rdi, greeting	; print message
	call print_string
	call print_newline

	mov rdi, input
	mov rsi, BUFFER_CAPACITY
	call read_word
	push rdx	; save string length
	test rax, rax
	je .overflow

	mov rdi, rax
	mov rsi, position
	call find_word
	test rax, rax
	je .notfound

	push rax
	mov rdi, found	; print success message
	call print_string
	call print_newline
	pop rax
	add rax, OFFSET
	mov rdi, rax	; print found key
	push rdi
	call print_string
	pop rdi
	
	pop rdx
	add rdi, rdx	
	inc rdi		; get value adress
	call print_string
	call print_newline
	xor rdi, rdi
	call exit

.overflow:
	mov rdi, overflow
	jmp .print_error
.notfound:
	mov rdi, notfound
.print_error:
	call print_error
	call print_newline
	mov rdi, 1
	call exit


